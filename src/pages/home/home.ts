import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  formA: FormGroup;

  constructor(
    public navCtrl: NavController,
    private http: HttpClient,
    formBuilder: FormBuilder
  ) {
    this.formA = formBuilder.group({});
  }

  limbeh() {
    // alert("ahahhahahaa");
    this.http.post(
      'https://www.alliancechat.com.my/webchat/AOLChat.aspx',
      {},
      { responseType: 'text' }
    ).subscribe(
      response => {
        console.log(response);
        let browserWindow = window.open('', '_system');
        browserWindow.document.write(response);
        // let r: any = response;
        // window.open('', '_system', r);
        // r.target = '_system'
      },
      error => {
        console.error(error);
      }
    );
  }

}
